<?php
/**
 * Displays footer site info
 *
 * @package ithdemo
 * 
 * 
 * 
 */

?>
<div class="site-info">
	<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'ithdemo' ) ); ?>"><?php printf( __( 'Proudly powered by %s', 'ithdemo' ), 'WordPress' ); ?></a>
</div><!-- .site-info -->
